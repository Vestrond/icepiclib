__author__ = 'CSH'

url = {'regular': 'http://icepic.ru/',
       'safe_regular': 'https://icepic.ru/',
       'image_storage': 'http://a.icepic.ru/',
       'api': 'http://icepic.ru/api/pic/'}

reg_exp = {"url_decoder": r'^(http://|)(www\.|)(a\.|)icepic\.ru((/medium|/thumbnail|)/(?P<sname>[A-z0-9]+)\.[a-z]+|/image/(?P<name>[a-z0-9]+))$'}