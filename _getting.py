__author__ = 'CSH'
import requests
import re
from bs4 import BeautifulSoup
import icepiclib._image_worker as iw
import icepiclib._data as ice_data


# 'http://a.icepic.ru/medium/f273539.png' -> 'f273539'
# 'http://a.icepic.ru/f273539.jpeg' -> 'f273539'
# etc...
def __url_decode__(url):
    reg = ice_data.reg_exp['url_decoder']

    result = (re.compile(reg)).search(url)

    if result.groupdict()['name'] or result.groupdict()['sname']:
        return result.groupdict()['name'] or result.groupdict()['sname']
    else:
        return False


# Load image as bytes. Return 'requests' py-lib
def __load_image__(url):
    file = requests.get(ice_data.url['image_storage'] + url)
    if file.ok:
        return file
    else:
        return False


# Load page and find current filename like 'f273539.png'
def __get_extension__(ice_hash):
    json_data = requests.get("{}{}/".format(ice_data.url['api'], ice_hash), params={'format':'json'})

    if json_data.ok:
        return json_data.json()['type']
    else:
        return False


# Get page's or image's info, as len, type, etc...
def __get_head__(url):
    info = requests.head(ice_data.url['image_storage'] + url)
    return info


# Get image's width & height, using only first 24 byte of file. Return [w, h]
def __get_borders__(url):
    # http://icepic.ru/api/pic/?user=&image_url=&thumbnail_url=&medium_url=
    if 'medium' in url:
        json_data = requests.get(ice_data.url['api'], params={'medium_url': url})
    elif 'thumbnail' in url:
        json_data = requests.get(ice_data.url['api'], params={'thumbnail_url': url})
    else:
        json_data = requests.get(ice_data.url['api'], params={'image_url': url})

    if json_data.ok:
        json_raw = json_data.json()['results'][0]

        if 'medium' in url:
            size = (json_raw['image_width'], json_raw['image_height'])
        elif 'thumbnail' in url:
            size = (json_raw['thumbnail_width'], json_raw['thumbnail_height'])
        else:
            size = (json_raw['image_width'], json_raw['image_height'])

        return size
    else:
        return False
