__author__ = 'CSH'
from PIL import Image
from io import BytesIO


# open file as byte and return byte_file
def open_as_byte(file):
    ft = open(file, 'rb')
    img = ft.read()
    ft.close()

    return img


# byte -> (PIL) image
def convert_from_byte(byte_file):
    img = Image.open(BytesIO(byte_file))
    return img


# (PIL) image -> size (width & height)
def get_size(byte_file):
    img = convert_from_byte(byte_file)
    return img.size


# if only_format = True
# byte_file[:26] -> get_file_format_26
# get_file_format_26 -> file format ('gif' or 'png' or ...)
def get_file_format_26(raw_byte):
    size = len(raw_byte)
    # GIF
    if (size >= 10) and raw_byte[:6] in (b"GIF87a", b"GIF89a"):
        return 'gif'

    # PNG
    elif ((size >= 24) and raw_byte.startswith(b'\x89PNG\r\n\x1a\n')
          and (raw_byte[12:16] == b"IHDR")):
        return 'png'

    # PNG OLD
    elif (size >= 16) and raw_byte.startswith(b'\x89PNG\r\n\x1a\n'):
        return 'png'

    # JPEG / JPG
    elif (size >= 2) and raw_byte.startswith(b"\xff\xd8"):
        return 'jpg'

    # BMP
    elif (size >= 4) and raw_byte.startswith(b"BM6+"):
        return 'bmp'

    else:
        print('{}:: Not analysed'.format(str(raw_byte)))
        return False


