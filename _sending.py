__author__ = 'CSH'
import requests
import uuid
import icepiclib._data as ice_data
import icepiclib._exceptions as exeptions

__upload_url__ = 'upload'

__mime_lib__ = {'png': 'image/png',
                'jpeg': 'image/jpeg',
                'jpg': 'image/jpeg',
                'gif': 'image/gif',
                'bmp': 'image/bmp',
                'wbmp': 'image/vnd.wap.wbmp',
                'ico': 'image/vnd.microsoft.icon',
                'tiff': 'image/tiff'}


# main function
def upload(file, filename):
    mime = __mime_lib__[filename.lower().split('.')[-1]]

    files = {}
    files['file'] = (filename, file, mime)

    data = {}
    data['uuid'] = __gen_uuid__()
    data['filename'] = filename
    data['totalfilesize'] = len(file)

    answer = requests.post("{}{}".format(ice_data.url['safe_regular'], __upload_url__), files=files, data=data)

    if answer.ok:
        if answer.json()['success']:
            return_data = {}

            return_data['ice_hash'] = answer.json()['hash']
            return_data['delete_url'] = 'image/{}'.format(answer.json()['newUuid'])

            return return_data

    raise exeptions.IcepicError(answer.raw, answer.reason)


# Generate UUID from 'host & time' cute
def __gen_uuid__():
    _uuid = uuid.uuid1()
    return str(_uuid)
