__author__ = 'CSH'

import icepiclib._sending as send
import icepiclib._getting as get
import icepiclib._image_worker as iw
import icepiclib._data as ice_data


class IceImage:
    # Hash from icepic db (str)
    ice_hash = None                              # Icepic image hash (str). Like "c6a8867"
    # Special url (url-str)
    __page_url__ = None                              # Url to view images (url-str). Like 'image/c2a8767'
    __delete_url__ = None                            # Url to delete image (url-str). Like 'image/c2a8867'
    # Images (__SubImage__)
    original = None                              # Image with real size and original extension (SubImage)
    medium = None                                # Medium image, having 'png' extension (SubImage)
    thumbnail = None                             # Small image, having 'png' extension (SubImage)
    # HIDDEN
    # URLs' templates. (str + {})
    __template_page_url__ = "image/{}"
    __template_original_url__ = "{}.{}"
    __template_medium_url__ = "medium/{}.png"
    __template_thumbnail_url__ = "thumbnail/{}.png"
    # Other data (str)
    __original_extension__ = None                # Original image's extension (str). Like 'png', 'jpeg', ...

    # Example:
    # IceImage()                                                         Empty
    # IceImage('c6a8867')                                                Hash
    # IceImage().from_file('C:/test.png', [name='Example'])              Local file. Name optionally.
    # IceImage().from_byte_file(b'...', [name='Example'])                Bytes file. Name optionally.
    # IceImage().from_url('http://icepic.ru/image/c6a8867')              Any links lite these
    #                      http://a.icepic.ru/c6a8867.jpeg
    #                      http://a.icepic.ru/medium/c6a8867.png
    #                      http://a.icepic.ru/thumbnail/c6a8867.png
    def __init__(self, ice_hash=None):
        if ice_hash:
            self.ice_hash = ice_hash
            self.__original_extension__ = get.__get_extension__(self.ice_hash)
            self.__collect_images__()

    #
    def from_file(self, file, name=None):
        if not self.ice_hash:
            # Open file as bytes
            byte_file = iw.open_as_byte(file)
            self.from_byte_file(byte_file, name)
        else:
            return False

    #
    def from_byte_file(self, byte_file, name=None):
        if not self.ice_hash:
            # Prepare SubImage
            self.__original_extension__ = iw.get_file_format_26(byte_file[:26])
            self.__upload__(byte_file, name)
            self.__collect_images__()
            # Fill all that we know
            self.original.__image__ = byte_file
            self.original.__size__ = len(byte_file)
            self.original.__width__, self.original.__height__ = iw.get_size(byte_file)
        else:
            return False

    #
    def from_url(self, url):
        if not self.ice_hash:
            self.ice_hash = get.__url_decode__(url)
            self.__original_extension__ = get.__get_extension__(self.ice_hash)
            self.__collect_images__()
        else:
            return False

    @property
    def page_url(self):
        if self.__page_url__:
            return ice_data.url['regular'] + self.__page_url__
        else:
            return None

    @property
    def delete_url(self):
        if self.__delete_url__:
            return ice_data.url['regular'] + self.__delete_url__
        else:
            return None

    def __collect_images__(self):
        self.__page_url__ = self.__template_page_url__.format(self.ice_hash)
        self.original = __SubImage__(self.__template_original_url__.format(self.ice_hash, self.__original_extension__))
        self.medium = __SubImage__(self.__template_medium_url__.format(self.ice_hash))
        self.thumbnail = __SubImage__(self.__template_thumbnail_url__.format(self.ice_hash))

    def __str__(self):
        return self.ice_hash or 'Empty_IceImage'

    # Upload file to IcePic
    def __upload__(self, byte_file, name=None):
        image_data = send.upload(byte_file, "{}.{}".format(name, self.__original_extension__))

        self.ice_hash = image_data['ice_hash']
        self.__delete_url__ = image_data['delete_url']


# class SubImage - view for each image
class __SubImage__:
    # Image and image's name
    __image__ = None                     # Image in bytes (bytes).
    __image_name__ = None                # Image's name (str). Like 'c6a8867.png'
    __image_format__ = None              # Image's format (str). Like 'png' or 'jpeg'
    __mime__ = None                      # Image's mime-type. Lite 'image/vnd.wap.wbmp' or 'image/bmp'
    # URL
    __url__ = None                       # Image's url (url-str). Like 'medium/c6a8867.png'
    # Size
    __width__ = None                     # Image's width (int). Like 200
    __height__ = None                    # Image's height (int). Like 100
    __size__ = None                      # Image's memory size in bytes (int). Like 12302

    def __init__(self, url):             # Pre-data: HASH, URL, NAME, FORMAT
        self.__url__ = url
        self.__ice_hash__ = get.__url_decode__(self.url)
        self.__image_name__ = url.split('\\')[-1].split('/')[-1]
        self.__image_format__ = self.__image_name__.split('.')[-1]

    def __str__(self):
        return self.__image_name__

    @property
    def image(self):
        if not self.__image__:
            img = get.__load_image__(self.__url__)
            self.__image__ = img.content
            self.__size__ = self.__size__ or int(img.headers['Content-Length'])
            self.__mime__ = self.__mime__ or img.headers['Content-Type']
        return self.__image__

    @property
    def name(self):                              # Pre-data
        return self.__image_name__

    @property
    def format(self):                            # Pre-data
        return self.__image_format__

    @property
    def mime(self):
        if not self.__mime__:
            self.__get_head__()
        return self.__mime__

    @property
    def url(self):                               # Pre-data
        return ice_data.url['image_storage'] + self.__url__

    @property
    def width(self):
        if not self.__width__:
            self.__width__, self.__height__ = get.__get_borders__(self.__url__)
        return self.__width__

    @property
    def height(self):
        if not self.__height__:
            self.__width__, self.__height__ = get.__get_borders__(self.__url__)
        return self.__height__

    @property
    def size(self):
        if not self.__size__:
            self.__get_head__()
        return self.__size__

    # Collect information about image (size and mime, without load image)
    def __get_head__(self):
        info = get.__get_head__(self.__url__)
        self.__size__ = int(info.headers['Content-Length'])
        self.__mime__ = self.__mime__ or info.headers['Content-Type']
