__author__ = 'CSH'
from icepiclib.iceimage import IceImage, __SubImage__
from icepiclib.test.tester import Tester


data_empty_init = {'str({})': 'Empty_IceImage',
                   '{}.ice_hash': None,
                   '{}.page_url': None,
                   '{}.delete_url': None,
                   '{}.original': None,
                   '{}.medium': None,
                   '{}.thumbnail': None,
                   }

data_hash_init = {'str({})': '294e6b5',
                   '{}.ice_hash': '294e6b5',
                   '{}.page_url': 'http://icepic.ru/image/294e6b5',
                   '{}.delete_url': None,
                   'str({}.original)': '294e6b5.jpeg',
                   'str({}.medium)': '294e6b5.png',
                   'str({}.thumbnail)': '294e6b5.png',
                   }

data_sub_image_init = {'str({})': '294e6b5.png',
                       'len({}.image)': 26353,
                       '{}.name': '294e6b5.png',
                       '{}.format': 'png',
                       '{}.mime': 'image/png',
                       '{}.url': "http://a.icepic.ru/medium/294e6b5.png",
                       '{}.width': 300,
                       '{}.height': 300,
                       '{}.size': 26353}

test_container =[{'lib': data_empty_init,
                  'name': 'IceImage.Init()',
                  'test_val': 'test_val(1)'},
                 {'lib': data_hash_init,
                  'name': 'IceImage.Init(hash)',
                  'test_val': 'test_val(2)'},
                 {'lib': data_sub_image_init,
                  'name': '__SubImage__.Init()',
                  'test_val': 'test_val(3)'}]


def test_val(id):
    if id == 1:
        return IceImage()
    elif id == 2:
        return IceImage('294e6b5')
    elif id == 3:
        return __SubImage__('medium/294e6b5.png')


def test():
    tester = Tester()
    print('\nStart test in module "iceimage.py"')

    for elem in test_container:
        tester.check_it(eval(elem['test_val']), elem['lib'], elem['name'])

    print('\nEnd test in module "iceimage.py"')
    print('Result: {} mismatches, {} critical errors.'.format(tester.mismatches, tester.critical_errors))