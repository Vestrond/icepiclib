__author__ = 'CSH'

from icepiclib.test.tester import Tester
import icepiclib._image_worker as iw

path = 'S:/Projects/Python/game_icon(subproj)/icepiclib/test/static/'
img_name = 'test'

img_path = '{}{}.'.format(path, img_name)

bmp_lib = {'len({}.open_as_byte("' + img_path + 'bmp"))': 11062,
           '{0}.convert_from_byte({0}.open_as_byte("' + img_path + 'bmp")).format_description': 'Windows Bitmap',
           '{0}.get_size({0}.open_as_byte("' + img_path + 'bmp"))': (128, 78),
           '{0}.get_file_format_26({0}.open_as_byte("' + img_path + 'bmp")[:26])': 'bmp'}

gif_lib = {'len({}.open_as_byte("' + img_path + 'gif"))': 19307,
           '{0}.convert_from_byte({0}.open_as_byte("' + img_path + 'gif")).format_description': 'Compuserve GIF',
           '{0}.get_size({0}.open_as_byte("' + img_path + 'gif"))': (90, 68),
           '{0}.get_file_format_26({0}.open_as_byte("' + img_path + 'gif")[:26])': 'gif'}

jpeg_lib = {'len({}.open_as_byte("' + img_path + 'jpeg"))': 10083,
            '{0}.convert_from_byte({0}.open_as_byte("' + img_path + 'jpeg")).format_description': 'JPEG (ISO 10918)',
            '{0}.get_size({0}.open_as_byte("' + img_path + 'jpeg"))': (64, 64),
            '{0}.get_file_format_26({0}.open_as_byte("' + img_path + 'jpeg")[:26])': 'jpg'}

jpg_lib = {'len({}.open_as_byte("' + img_path + 'jpg"))': 10083,
           '{0}.convert_from_byte({0}.open_as_byte("' + img_path + 'jpg")).format_description': 'JPEG (ISO 10918)',
           '{0}.get_size({0}.open_as_byte("' + img_path + 'jpg"))': (64, 64),
           '{0}.get_file_format_26({0}.open_as_byte("' + img_path + 'jpg")[:26])': 'jpg'}

png_lib = {'len({}.open_as_byte("' + img_path + 'png"))': 2210,
           '{0}.convert_from_byte({0}.open_as_byte("' + img_path + 'png")).format_description': 'Portable network graphics',
           '{0}.get_size({0}.open_as_byte("' + img_path + 'png"))': (64, 64),
           '{0}.get_file_format_26({0}.open_as_byte("' + img_path + 'png")[:26])': 'png'}


images = ['bmp', 'gif', 'jpeg', 'jpg', 'png']

test_container =[{'lib': bmp_lib,
                  'name': 'ImageWorker( bmp )',
                  'test_val': 'test_val()'},
                 {'lib': gif_lib,
                  'name': 'ImageWorker( gif )',
                  'test_val': 'test_val()'},
                 {'lib': jpeg_lib,
                  'name': 'ImageWorker( jpeg )',
                  'test_val': 'test_val()'},
                 {'lib': jpg_lib,
                  'name': 'ImageWorker( jpg )',
                  'test_val': 'test_val()'},
                 {'lib': png_lib,
                  'name': 'ImageWorker( png )',
                  'test_val': 'test_val()'}]


def test_val():
    return iw


def test():
    tester = Tester()
    print('\nStart test in module "_image_worker.py"')

    for elem in test_container:
        tester.check_it(eval(elem['test_val']), elem['lib'], elem['name'])

    print('\nEnd test in module "_image_worker.py"')
    print('Result: {} mismatches, {} critical errors.'.format(tester.mismatches, tester.critical_errors))
