__author__ = 'CSH'


class Tester:
    check_lib = None
    counter = None
    counter_errors = None

    mismatches = 0
    critical_errors = 0

    def __init__(self):
        pass

    def check_it(self, test_val, lib, name):
        print("\n  Beginning test: {}.".format(name))

        self.reset_counters()
        self.check_lib = lib

        for val in lib:
            try:
                eval_result = eval(val.format('test_val'))

                if eval_result == self.check_lib[val]:
                    print("    {} == {}..............OK".format(val.format("X"), self.check_lib[val]))
                    self.counter += 1
                else:
                    print("    {} == {}..............FAIL".format(val.format("X"), self.check_lib[val]))
                    print("   >{} == {}".format(val.format("X"), eval_result))
                    self.mismatches += 1
            except:
                self.counter_errors += 1
                self.critical_errors += 1
                print("    Critical error. Jump out.")
        print("  Test finish: {}. {}/{} OK. {} crit.errors.".format(name, self.counter, len(self.check_lib), self.counter_errors))

    def reset_counters(self):
        self.counter = 0
        self.counter_errors = 0
